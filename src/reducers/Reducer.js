//State
const initialState = {
    balance: 20000,
    name:"Ashok",
    accNo: 9030008242
}

const Reducer = (state = initialState,action)=>{
    switch(action.type){
        case "WITHDRAW":{
            return {
                ...state,
                balance: state.balance - 100
            }
        }
        case "DEPOSIT":{
            return {
                ...state,
                balance: state.balance + 100
            }
        }
        default:return state;
    }
}
export default Reducer;