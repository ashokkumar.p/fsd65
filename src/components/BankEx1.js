import React, { Component } from 'react'
import { connect } from 'react-redux'

class BankEx1 extends Component {
  render() {
    return (
      <div>
        <h3>Name: {this.props.name}</h3>
        <h3>Acc Number: {this.props.accNO}</h3>
        <h3>Balance: {this.props.balance}</h3>
        <button onClick={this.props.deposite}>Deposit</button>
        <button onClick={this.props.withdraw}>Withdraw</button>
      </div>
    )
  }
}
const mapStateToProps= (state)=>{
    return{
        balance: state.balance,
        name:state.name,
        accNO: state.accNo
    }
}
const mapDispatchToProps = (dispatch)=>{
    return{
        deposite:()=>(dispatch({type:"DEPOSIT"})),
        withdraw:()=>(dispatch({type:"WITHDRAW"}))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(BankEx1)